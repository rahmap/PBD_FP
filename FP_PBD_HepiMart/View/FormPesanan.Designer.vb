﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPesanan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPesanan))
        Dim StateProperties1 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties2 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties3 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties4 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties5 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties6 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties7 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties8 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.PanelNav = New System.Windows.Forms.Panel()
        Me.btnMini = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnExit = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.HepiMart = New Bunifu.UI.WinForms.BunifuLabel()
        Me.PanelLeftSidebar = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnHome = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuProgressBar1 = New Bunifu.UI.WinForms.BunifuProgressBar()
        Me.btnDriver = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnMenu = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnPelanggan = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnTransaksi = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnPesanan = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnLogout = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.BunifuElipse1 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbAlamat = New Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbTelp = New Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbIDpel = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbNama = New Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblStok = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.BunifuProgressBar2 = New Bunifu.UI.WinForms.BunifuProgressBar()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cbMenu = New Bunifu.UI.WinForms.BunifuDropdown()
        Me.jmlPesan = New System.Windows.Forms.NumericUpDown()
        Me.cbDriver = New Bunifu.UI.WinForms.BunifuDropdown()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbIDPesanan = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbSubHarga = New System.Windows.Forms.TextBox()
        Me.rbMakanan = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.rbMinuman = New System.Windows.Forms.RadioButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnInsertPlg = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.btnResetPesanan = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.btnInput = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.btnResetPelanggan = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.dgvPemesanan = New Bunifu.UI.WinForms.BunifuDataGridView()
        Me.tbTotalHarga = New Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox()
        Me.DTPWaktu = New System.Windows.Forms.DateTimePicker()
        Me.PanelNav.SuspendLayout()
        Me.PanelLeftSidebar.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.jmlPesan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvPemesanan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelNav
        '
        Me.PanelNav.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.PanelNav.Controls.Add(Me.btnMini)
        Me.PanelNav.Controls.Add(Me.btnExit)
        Me.PanelNav.Controls.Add(Me.HepiMart)
        Me.PanelNav.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelNav.Location = New System.Drawing.Point(0, 0)
        Me.PanelNav.Name = "PanelNav"
        Me.PanelNav.Size = New System.Drawing.Size(1036, 45)
        Me.PanelNav.TabIndex = 3
        '
        'btnMini
        '
        Me.btnMini.Activecolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnMini.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnMini.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMini.BorderRadius = 0
        Me.btnMini.ButtonText = ""
        Me.btnMini.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMini.DisabledColor = System.Drawing.Color.Gray
        Me.btnMini.Iconcolor = System.Drawing.Color.Transparent
        Me.btnMini.Iconimage = CType(resources.GetObject("btnMini.Iconimage"), System.Drawing.Image)
        Me.btnMini.Iconimage_right = Nothing
        Me.btnMini.Iconimage_right_Selected = Nothing
        Me.btnMini.Iconimage_Selected = Nothing
        Me.btnMini.IconMarginLeft = 0
        Me.btnMini.IconMarginRight = 0
        Me.btnMini.IconRightVisible = True
        Me.btnMini.IconRightZoom = 0R
        Me.btnMini.IconVisible = True
        Me.btnMini.IconZoom = 90.0R
        Me.btnMini.IsTab = False
        Me.btnMini.Location = New System.Drawing.Point(945, 4)
        Me.btnMini.Name = "btnMini"
        Me.btnMini.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnMini.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnMini.OnHoverTextColor = System.Drawing.Color.White
        Me.btnMini.selected = False
        Me.btnMini.Size = New System.Drawing.Size(46, 39)
        Me.btnMini.TabIndex = 5
        Me.btnMini.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMini.Textcolor = System.Drawing.Color.White
        Me.btnMini.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'btnExit
        '
        Me.btnExit.Activecolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnExit.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnExit.BorderRadius = 0
        Me.btnExit.ButtonText = ""
        Me.btnExit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnExit.DisabledColor = System.Drawing.Color.Gray
        Me.btnExit.Iconcolor = System.Drawing.Color.Transparent
        Me.btnExit.Iconimage = CType(resources.GetObject("btnExit.Iconimage"), System.Drawing.Image)
        Me.btnExit.Iconimage_right = Nothing
        Me.btnExit.Iconimage_right_Selected = Nothing
        Me.btnExit.Iconimage_Selected = Nothing
        Me.btnExit.IconMarginLeft = 0
        Me.btnExit.IconMarginRight = 0
        Me.btnExit.IconRightVisible = True
        Me.btnExit.IconRightZoom = 0R
        Me.btnExit.IconVisible = True
        Me.btnExit.IconZoom = 90.0R
        Me.btnExit.IsTab = False
        Me.btnExit.Location = New System.Drawing.Point(987, 4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnExit.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnExit.OnHoverTextColor = System.Drawing.Color.White
        Me.btnExit.selected = False
        Me.btnExit.Size = New System.Drawing.Size(46, 39)
        Me.btnExit.TabIndex = 4
        Me.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExit.Textcolor = System.Drawing.Color.White
        Me.btnExit.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'HepiMart
        '
        Me.HepiMart.AutoEllipsis = False
        Me.HepiMart.CursorType = Nothing
        Me.HepiMart.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.HepiMart.Location = New System.Drawing.Point(505, 12)
        Me.HepiMart.Name = "HepiMart"
        Me.HepiMart.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.HepiMart.Size = New System.Drawing.Size(80, 23)
        Me.HepiMart.TabIndex = 3
        Me.HepiMart.Text = "Hepi Mart"
        Me.HepiMart.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.HepiMart.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'PanelLeftSidebar
        '
        Me.PanelLeftSidebar.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.PanelLeftSidebar.Controls.Add(Me.Panel2)
        Me.PanelLeftSidebar.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelLeftSidebar.Location = New System.Drawing.Point(0, 45)
        Me.PanelLeftSidebar.Name = "PanelLeftSidebar"
        Me.PanelLeftSidebar.Size = New System.Drawing.Size(175, 585)
        Me.PanelLeftSidebar.TabIndex = 4
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Panel2.Controls.Add(Me.btnHome)
        Me.Panel2.Controls.Add(Me.BunifuProgressBar1)
        Me.Panel2.Controls.Add(Me.btnDriver)
        Me.Panel2.Controls.Add(Me.btnMenu)
        Me.Panel2.Controls.Add(Me.btnPelanggan)
        Me.Panel2.Controls.Add(Me.btnTransaksi)
        Me.Panel2.Controls.Add(Me.btnPesanan)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(175, 585)
        Me.Panel2.TabIndex = 1
        '
        'btnHome
        '
        Me.btnHome.Activecolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnHome.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHome.BorderRadius = 0
        Me.btnHome.ButtonText = "Dashboard"
        Me.btnHome.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnHome.DisabledColor = System.Drawing.Color.Gray
        Me.btnHome.Iconcolor = System.Drawing.Color.Transparent
        Me.btnHome.Iconimage = CType(resources.GetObject("btnHome.Iconimage"), System.Drawing.Image)
        Me.btnHome.Iconimage_right = Nothing
        Me.btnHome.Iconimage_right_Selected = Nothing
        Me.btnHome.Iconimage_Selected = Nothing
        Me.btnHome.IconMarginLeft = 0
        Me.btnHome.IconMarginRight = 0
        Me.btnHome.IconRightVisible = True
        Me.btnHome.IconRightZoom = 0R
        Me.btnHome.IconVisible = True
        Me.btnHome.IconZoom = 90.0R
        Me.btnHome.IsTab = False
        Me.btnHome.Location = New System.Drawing.Point(-1, 42)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnHome.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnHome.OnHoverTextColor = System.Drawing.Color.White
        Me.btnHome.selected = False
        Me.btnHome.Size = New System.Drawing.Size(176, 48)
        Me.btnHome.TabIndex = 22
        Me.btnHome.Text = "Dashboard"
        Me.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHome.Textcolor = System.Drawing.Color.Black
        Me.btnHome.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'BunifuProgressBar1
        '
        Me.BunifuProgressBar1.Animation = 0
        Me.BunifuProgressBar1.AnimationStep = 10
        Me.BunifuProgressBar1.BackgroundImage = CType(resources.GetObject("BunifuProgressBar1.BackgroundImage"), System.Drawing.Image)
        Me.BunifuProgressBar1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.BunifuProgressBar1.BorderRadius = 5
        Me.BunifuProgressBar1.BorderThickness = 2
        Me.BunifuProgressBar1.Location = New System.Drawing.Point(-8, 531)
        Me.BunifuProgressBar1.MaximumValue = 100
        Me.BunifuProgressBar1.MinimumValue = 0
        Me.BunifuProgressBar1.Name = "BunifuProgressBar1"
        Me.BunifuProgressBar1.ProgressBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.BunifuProgressBar1.ProgressColorLeft = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(181, Byte), Integer))
        Me.BunifuProgressBar1.ProgressColorRight = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(181, Byte), Integer))
        Me.BunifuProgressBar1.Size = New System.Drawing.Size(183, 10)
        Me.BunifuProgressBar1.TabIndex = 3
        Me.BunifuProgressBar1.Value = 0
        '
        'btnDriver
        '
        Me.btnDriver.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnDriver.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnDriver.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDriver.BorderRadius = 0
        Me.btnDriver.ButtonText = "Driver"
        Me.btnDriver.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDriver.DisabledColor = System.Drawing.Color.Gray
        Me.btnDriver.Iconcolor = System.Drawing.Color.Transparent
        Me.btnDriver.Iconimage = CType(resources.GetObject("btnDriver.Iconimage"), System.Drawing.Image)
        Me.btnDriver.Iconimage_right = Nothing
        Me.btnDriver.Iconimage_right_Selected = Nothing
        Me.btnDriver.Iconimage_Selected = Nothing
        Me.btnDriver.IconMarginLeft = 0
        Me.btnDriver.IconMarginRight = 0
        Me.btnDriver.IconRightVisible = True
        Me.btnDriver.IconRightZoom = 0R
        Me.btnDriver.IconVisible = True
        Me.btnDriver.IconZoom = 90.0R
        Me.btnDriver.IsTab = False
        Me.btnDriver.Location = New System.Drawing.Point(0, 312)
        Me.btnDriver.Name = "btnDriver"
        Me.btnDriver.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnDriver.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnDriver.OnHoverTextColor = System.Drawing.Color.White
        Me.btnDriver.selected = False
        Me.btnDriver.Size = New System.Drawing.Size(175, 48)
        Me.btnDriver.TabIndex = 21
        Me.btnDriver.Text = "Driver"
        Me.btnDriver.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDriver.Textcolor = System.Drawing.Color.Black
        Me.btnDriver.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold)
        '
        'btnMenu
        '
        Me.btnMenu.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMenu.BorderRadius = 0
        Me.btnMenu.ButtonText = "Menu"
        Me.btnMenu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMenu.DisabledColor = System.Drawing.Color.Gray
        Me.btnMenu.Iconcolor = System.Drawing.Color.Transparent
        Me.btnMenu.Iconimage = CType(resources.GetObject("btnMenu.Iconimage"), System.Drawing.Image)
        Me.btnMenu.Iconimage_right = Nothing
        Me.btnMenu.Iconimage_right_Selected = Nothing
        Me.btnMenu.Iconimage_Selected = Nothing
        Me.btnMenu.IconMarginLeft = 0
        Me.btnMenu.IconMarginRight = 0
        Me.btnMenu.IconRightVisible = True
        Me.btnMenu.IconRightZoom = 0R
        Me.btnMenu.IconVisible = True
        Me.btnMenu.IconZoom = 90.0R
        Me.btnMenu.IsTab = False
        Me.btnMenu.Location = New System.Drawing.Point(0, 258)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnMenu.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnMenu.OnHoverTextColor = System.Drawing.Color.White
        Me.btnMenu.selected = False
        Me.btnMenu.Size = New System.Drawing.Size(175, 48)
        Me.btnMenu.TabIndex = 20
        Me.btnMenu.Text = "Menu"
        Me.btnMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu.Textcolor = System.Drawing.Color.Black
        Me.btnMenu.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold)
        '
        'btnPelanggan
        '
        Me.btnPelanggan.Activecolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnPelanggan.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnPelanggan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPelanggan.BorderRadius = 0
        Me.btnPelanggan.ButtonText = "Pelanggan"
        Me.btnPelanggan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPelanggan.DisabledColor = System.Drawing.Color.Gray
        Me.btnPelanggan.Iconcolor = System.Drawing.Color.Transparent
        Me.btnPelanggan.Iconimage = CType(resources.GetObject("btnPelanggan.Iconimage"), System.Drawing.Image)
        Me.btnPelanggan.Iconimage_right = Nothing
        Me.btnPelanggan.Iconimage_right_Selected = Nothing
        Me.btnPelanggan.Iconimage_Selected = Nothing
        Me.btnPelanggan.IconMarginLeft = 0
        Me.btnPelanggan.IconMarginRight = 0
        Me.btnPelanggan.IconRightVisible = True
        Me.btnPelanggan.IconRightZoom = 0R
        Me.btnPelanggan.IconVisible = True
        Me.btnPelanggan.IconZoom = 90.0R
        Me.btnPelanggan.IsTab = False
        Me.btnPelanggan.Location = New System.Drawing.Point(0, 204)
        Me.btnPelanggan.Name = "btnPelanggan"
        Me.btnPelanggan.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnPelanggan.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnPelanggan.OnHoverTextColor = System.Drawing.Color.White
        Me.btnPelanggan.selected = False
        Me.btnPelanggan.Size = New System.Drawing.Size(175, 48)
        Me.btnPelanggan.TabIndex = 19
        Me.btnPelanggan.Text = "Pelanggan"
        Me.btnPelanggan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPelanggan.Textcolor = System.Drawing.Color.Black
        Me.btnPelanggan.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold)
        '
        'btnTransaksi
        '
        Me.btnTransaksi.Activecolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnTransaksi.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnTransaksi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTransaksi.BorderRadius = 0
        Me.btnTransaksi.ButtonText = "Transaksi"
        Me.btnTransaksi.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnTransaksi.DisabledColor = System.Drawing.Color.Gray
        Me.btnTransaksi.Iconcolor = System.Drawing.Color.Transparent
        Me.btnTransaksi.Iconimage = CType(resources.GetObject("btnTransaksi.Iconimage"), System.Drawing.Image)
        Me.btnTransaksi.Iconimage_right = Nothing
        Me.btnTransaksi.Iconimage_right_Selected = Nothing
        Me.btnTransaksi.Iconimage_Selected = Nothing
        Me.btnTransaksi.IconMarginLeft = 0
        Me.btnTransaksi.IconMarginRight = 0
        Me.btnTransaksi.IconRightVisible = True
        Me.btnTransaksi.IconRightZoom = 0R
        Me.btnTransaksi.IconVisible = True
        Me.btnTransaksi.IconZoom = 90.0R
        Me.btnTransaksi.IsTab = False
        Me.btnTransaksi.Location = New System.Drawing.Point(-1, 150)
        Me.btnTransaksi.Name = "btnTransaksi"
        Me.btnTransaksi.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnTransaksi.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnTransaksi.OnHoverTextColor = System.Drawing.Color.White
        Me.btnTransaksi.selected = False
        Me.btnTransaksi.Size = New System.Drawing.Size(176, 48)
        Me.btnTransaksi.TabIndex = 18
        Me.btnTransaksi.Text = "Transaksi"
        Me.btnTransaksi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTransaksi.Textcolor = System.Drawing.Color.Black
        Me.btnTransaksi.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold)
        '
        'btnPesanan
        '
        Me.btnPesanan.Activecolor = System.Drawing.Color.SeaGreen
        Me.btnPesanan.BackColor = System.Drawing.Color.SeaGreen
        Me.btnPesanan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPesanan.BorderRadius = 0
        Me.btnPesanan.ButtonText = "Pesanan"
        Me.btnPesanan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPesanan.DisabledColor = System.Drawing.Color.Gray
        Me.btnPesanan.Iconcolor = System.Drawing.Color.Transparent
        Me.btnPesanan.Iconimage = CType(resources.GetObject("btnPesanan.Iconimage"), System.Drawing.Image)
        Me.btnPesanan.Iconimage_right = Nothing
        Me.btnPesanan.Iconimage_right_Selected = Nothing
        Me.btnPesanan.Iconimage_Selected = Nothing
        Me.btnPesanan.IconMarginLeft = 0
        Me.btnPesanan.IconMarginRight = 0
        Me.btnPesanan.IconRightVisible = True
        Me.btnPesanan.IconRightZoom = 0R
        Me.btnPesanan.IconVisible = True
        Me.btnPesanan.IconZoom = 90.0R
        Me.btnPesanan.IsTab = False
        Me.btnPesanan.Location = New System.Drawing.Point(0, 96)
        Me.btnPesanan.Name = "btnPesanan"
        Me.btnPesanan.Normalcolor = System.Drawing.Color.SeaGreen
        Me.btnPesanan.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnPesanan.OnHoverTextColor = System.Drawing.Color.White
        Me.btnPesanan.selected = False
        Me.btnPesanan.Size = New System.Drawing.Size(175, 48)
        Me.btnPesanan.TabIndex = 17
        Me.btnPesanan.Text = "Pesanan"
        Me.btnPesanan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPesanan.Textcolor = System.Drawing.Color.Black
        Me.btnPesanan.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnLogout)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 531)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(175, 54)
        Me.Panel1.TabIndex = 16
        '
        'btnLogout
        '
        Me.btnLogout.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnLogout.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnLogout.BorderRadius = 0
        Me.btnLogout.ButtonText = "Log Out"
        Me.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLogout.DisabledColor = System.Drawing.Color.Gray
        Me.btnLogout.Font = New System.Drawing.Font("Century Gothic", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogout.Iconcolor = System.Drawing.Color.Transparent
        Me.btnLogout.Iconimage = Nothing
        Me.btnLogout.Iconimage_right = Nothing
        Me.btnLogout.Iconimage_right_Selected = Nothing
        Me.btnLogout.Iconimage_Selected = Nothing
        Me.btnLogout.IconMarginLeft = 0
        Me.btnLogout.IconMarginRight = 0
        Me.btnLogout.IconRightVisible = True
        Me.btnLogout.IconRightZoom = 0R
        Me.btnLogout.IconVisible = True
        Me.btnLogout.IconZoom = 90.0R
        Me.btnLogout.IsTab = False
        Me.btnLogout.Location = New System.Drawing.Point(0, 4)
        Me.btnLogout.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnLogout.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnLogout.OnHoverTextColor = System.Drawing.Color.White
        Me.btnLogout.selected = False
        Me.btnLogout.Size = New System.Drawing.Size(176, 50)
        Me.btnLogout.TabIndex = 0
        Me.btnLogout.Text = "Log Out"
        Me.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnLogout.Textcolor = System.Drawing.Color.Black
        Me.btnLogout.TextFont = New System.Drawing.Font("Century Gothic", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.PanelNav
        Me.BunifuDragControl1.Vertical = True
        '
        'BunifuElipse1
        '
        Me.BunifuElipse1.ElipseRadius = 5
        Me.BunifuElipse1.TargetControl = Me
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label4.Location = New System.Drawing.Point(11, 153)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Alamat Pelanggan"
        '
        'tbAlamat
        '
        Me.tbAlamat.AcceptsReturn = False
        Me.tbAlamat.AcceptsTab = False
        Me.tbAlamat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.tbAlamat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.tbAlamat.BackColor = System.Drawing.Color.Transparent
        Me.tbAlamat.BackgroundImage = CType(resources.GetObject("tbAlamat.BackgroundImage"), System.Drawing.Image)
        Me.tbAlamat.BorderColorActive = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(145, Byte), Integer))
        Me.tbAlamat.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.tbAlamat.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.tbAlamat.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.tbAlamat.BorderRadius = 1
        Me.tbAlamat.BorderThickness = 2
        Me.tbAlamat.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.tbAlamat.DefaultFont = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.tbAlamat.DefaultText = ""
        Me.tbAlamat.FillColor = System.Drawing.Color.White
        Me.tbAlamat.HideSelection = True
        Me.tbAlamat.IconLeft = Nothing
        Me.tbAlamat.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.tbAlamat.IconPadding = 10
        Me.tbAlamat.IconRight = Nothing
        Me.tbAlamat.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.tbAlamat.Location = New System.Drawing.Point(106, 145)
        Me.tbAlamat.MaxLength = 32767
        Me.tbAlamat.MinimumSize = New System.Drawing.Size(100, 35)
        Me.tbAlamat.Modified = False
        Me.tbAlamat.Name = "tbAlamat"
        Me.tbAlamat.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.tbAlamat.ReadOnly = False
        Me.tbAlamat.SelectedText = ""
        Me.tbAlamat.SelectionLength = 0
        Me.tbAlamat.SelectionStart = 0
        Me.tbAlamat.ShortcutsEnabled = True
        Me.tbAlamat.Size = New System.Drawing.Size(146, 35)
        Me.tbAlamat.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu
        Me.tbAlamat.TabIndex = 7
        Me.tbAlamat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.tbAlamat.TextMarginLeft = 5
        Me.tbAlamat.TextPlaceholder = ""
        Me.tbAlamat.UseSystemPasswordChar = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label3.Location = New System.Drawing.Point(11, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Telp Pelanggan"
        '
        'tbTelp
        '
        Me.tbTelp.AcceptsReturn = False
        Me.tbTelp.AcceptsTab = False
        Me.tbTelp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.tbTelp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.tbTelp.BackColor = System.Drawing.Color.Transparent
        Me.tbTelp.BackgroundImage = CType(resources.GetObject("tbTelp.BackgroundImage"), System.Drawing.Image)
        Me.tbTelp.BorderColorActive = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(145, Byte), Integer))
        Me.tbTelp.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.tbTelp.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.tbTelp.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.tbTelp.BorderRadius = 1
        Me.tbTelp.BorderThickness = 2
        Me.tbTelp.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.tbTelp.DefaultFont = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.tbTelp.DefaultText = ""
        Me.tbTelp.FillColor = System.Drawing.Color.White
        Me.tbTelp.HideSelection = True
        Me.tbTelp.IconLeft = Nothing
        Me.tbTelp.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.tbTelp.IconPadding = 10
        Me.tbTelp.IconRight = Nothing
        Me.tbTelp.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.tbTelp.Location = New System.Drawing.Point(106, 104)
        Me.tbTelp.MaxLength = 32767
        Me.tbTelp.MinimumSize = New System.Drawing.Size(100, 35)
        Me.tbTelp.Modified = False
        Me.tbTelp.Name = "tbTelp"
        Me.tbTelp.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.tbTelp.ReadOnly = False
        Me.tbTelp.SelectedText = ""
        Me.tbTelp.SelectionLength = 0
        Me.tbTelp.SelectionStart = 0
        Me.tbTelp.ShortcutsEnabled = True
        Me.tbTelp.Size = New System.Drawing.Size(146, 35)
        Me.tbTelp.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu
        Me.tbTelp.TabIndex = 5
        Me.tbTelp.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.tbTelp.TextMarginLeft = 5
        Me.tbTelp.TextPlaceholder = ""
        Me.tbTelp.UseSystemPasswordChar = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.tbIDpel)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.tbNama)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.tbTelp)
        Me.GroupBox1.Controls.Add(Me.tbAlamat)
        Me.GroupBox1.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Italic)
        Me.GroupBox1.Location = New System.Drawing.Point(194, 87)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(272, 196)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Pelanggan"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label1.Location = New System.Drawing.Point(83, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "ID Pelanggan"
        '
        'tbIDpel
        '
        Me.tbIDpel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.tbIDpel.Enabled = False
        Me.tbIDpel.Location = New System.Drawing.Point(161, 26)
        Me.tbIDpel.Name = "tbIDpel"
        Me.tbIDpel.Size = New System.Drawing.Size(91, 22)
        Me.tbIDpel.TabIndex = 25
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label2.Location = New System.Drawing.Point(11, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Nama Pelanggan"
        '
        'tbNama
        '
        Me.tbNama.AcceptsReturn = False
        Me.tbNama.AcceptsTab = False
        Me.tbNama.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.tbNama.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.tbNama.BackColor = System.Drawing.Color.Transparent
        Me.tbNama.BackgroundImage = CType(resources.GetObject("tbNama.BackgroundImage"), System.Drawing.Image)
        Me.tbNama.BorderColorActive = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(145, Byte), Integer))
        Me.tbNama.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.tbNama.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.tbNama.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.tbNama.BorderRadius = 1
        Me.tbNama.BorderThickness = 2
        Me.tbNama.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.tbNama.DefaultFont = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.tbNama.DefaultText = ""
        Me.tbNama.FillColor = System.Drawing.Color.White
        Me.tbNama.HideSelection = True
        Me.tbNama.IconLeft = Nothing
        Me.tbNama.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.tbNama.IconPadding = 10
        Me.tbNama.IconRight = Nothing
        Me.tbNama.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.tbNama.Location = New System.Drawing.Point(106, 61)
        Me.tbNama.MaxLength = 32767
        Me.tbNama.MinimumSize = New System.Drawing.Size(100, 35)
        Me.tbNama.Modified = False
        Me.tbNama.Name = "tbNama"
        Me.tbNama.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.tbNama.ReadOnly = False
        Me.tbNama.SelectedText = ""
        Me.tbNama.SelectionLength = 0
        Me.tbNama.SelectionStart = 0
        Me.tbNama.ShortcutsEnabled = True
        Me.tbNama.Size = New System.Drawing.Size(146, 35)
        Me.tbNama.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu
        Me.tbNama.TabIndex = 10
        Me.tbNama.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.tbNama.TextMarginLeft = 5
        Me.tbNama.TextPlaceholder = ""
        Me.tbNama.UseSystemPasswordChar = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblStok)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.BunifuProgressBar2)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.cbMenu)
        Me.GroupBox2.Controls.Add(Me.jmlPesan)
        Me.GroupBox2.Controls.Add(Me.cbDriver)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.tbIDPesanan)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.tbSubHarga)
        Me.GroupBox2.Controls.Add(Me.rbMakanan)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.rbMinuman)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Italic)
        Me.GroupBox2.Location = New System.Drawing.Point(491, 87)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(277, 264)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Data Pesanan"
        '
        'lblStok
        '
        Me.lblStok.AutoSize = True
        Me.lblStok.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStok.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblStok.Location = New System.Drawing.Point(236, 183)
        Me.lblStok.Name = "lblStok"
        Me.lblStok.Size = New System.Drawing.Size(14, 13)
        Me.lblStok.TabIndex = 30
        Me.lblStok.Text = "0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label7.Location = New System.Drawing.Point(194, 183)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(29, 13)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Stok"
        '
        'BunifuProgressBar2
        '
        Me.BunifuProgressBar2.Animation = 0
        Me.BunifuProgressBar2.AnimationStep = 10
        Me.BunifuProgressBar2.BackgroundImage = CType(resources.GetObject("BunifuProgressBar2.BackgroundImage"), System.Drawing.Image)
        Me.BunifuProgressBar2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BunifuProgressBar2.BorderRadius = 5
        Me.BunifuProgressBar2.BorderThickness = 2
        Me.BunifuProgressBar2.Location = New System.Drawing.Point(6, 218)
        Me.BunifuProgressBar2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.BunifuProgressBar2.MaximumValue = 100
        Me.BunifuProgressBar2.MinimumValue = 0
        Me.BunifuProgressBar2.Name = "BunifuProgressBar2"
        Me.BunifuProgressBar2.ProgressBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.BunifuProgressBar2.ProgressColorLeft = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(181, Byte), Integer))
        Me.BunifuProgressBar2.ProgressColorRight = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(181, Byte), Integer))
        Me.BunifuProgressBar2.Size = New System.Drawing.Size(265, 10)
        Me.BunifuProgressBar2.TabIndex = 21
        Me.BunifuProgressBar2.Value = 0
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label12.Location = New System.Drawing.Point(20, 143)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(34, 13)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Menu"
        '
        'cbMenu
        '
        Me.cbMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.cbMenu.BorderRadius = 1
        Me.cbMenu.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down
        Me.cbMenu.DisabledColor = System.Drawing.Color.Gray
        Me.cbMenu.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbMenu.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thick
        Me.cbMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbMenu.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left
        Me.cbMenu.FillDropDown = False
        Me.cbMenu.FillIndicator = False
        Me.cbMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbMenu.ForeColor = System.Drawing.Color.Purple
        Me.cbMenu.FormattingEnabled = True
        Me.cbMenu.Icon = Nothing
        Me.cbMenu.IndicatorColor = System.Drawing.Color.Purple
        Me.cbMenu.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right
        Me.cbMenu.ItemBackColor = System.Drawing.Color.White
        Me.cbMenu.ItemBorderColor = System.Drawing.Color.White
        Me.cbMenu.ItemForeColor = System.Drawing.Color.Purple
        Me.cbMenu.ItemHeight = 26
        Me.cbMenu.ItemHighLightColor = System.Drawing.Color.Thistle
        Me.cbMenu.Location = New System.Drawing.Point(96, 134)
        Me.cbMenu.Name = "cbMenu"
        Me.cbMenu.Size = New System.Drawing.Size(165, 32)
        Me.cbMenu.TabIndex = 28
        Me.cbMenu.Text = Nothing
        '
        'jmlPesan
        '
        Me.jmlPesan.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.jmlPesan.Location = New System.Drawing.Point(96, 180)
        Me.jmlPesan.Name = "jmlPesan"
        Me.jmlPesan.Size = New System.Drawing.Size(92, 20)
        Me.jmlPesan.TabIndex = 26
        '
        'cbDriver
        '
        Me.cbDriver.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.cbDriver.BorderRadius = 1
        Me.cbDriver.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down
        Me.cbDriver.DisabledColor = System.Drawing.Color.Gray
        Me.cbDriver.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbDriver.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thick
        Me.cbDriver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbDriver.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left
        Me.cbDriver.FillDropDown = False
        Me.cbDriver.FillIndicator = False
        Me.cbDriver.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbDriver.ForeColor = System.Drawing.Color.Purple
        Me.cbDriver.FormattingEnabled = True
        Me.cbDriver.Icon = Nothing
        Me.cbDriver.IndicatorColor = System.Drawing.Color.Purple
        Me.cbDriver.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right
        Me.cbDriver.ItemBackColor = System.Drawing.Color.White
        Me.cbDriver.ItemBorderColor = System.Drawing.Color.White
        Me.cbDriver.ItemForeColor = System.Drawing.Color.Purple
        Me.cbDriver.ItemHeight = 26
        Me.cbDriver.ItemHighLightColor = System.Drawing.Color.Thistle
        Me.cbDriver.Location = New System.Drawing.Point(96, 61)
        Me.cbDriver.Name = "cbDriver"
        Me.cbDriver.Size = New System.Drawing.Size(165, 32)
        Me.cbDriver.TabIndex = 25
        Me.cbDriver.Text = Nothing
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label11.Location = New System.Drawing.Point(93, 31)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(63, 13)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "ID Pesanan"
        '
        'tbIDPesanan
        '
        Me.tbIDPesanan.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.tbIDPesanan.Enabled = False
        Me.tbIDPesanan.Location = New System.Drawing.Point(161, 26)
        Me.tbIDPesanan.Name = "tbIDPesanan"
        Me.tbIDPesanan.Size = New System.Drawing.Size(100, 22)
        Me.tbIDPesanan.TabIndex = 23
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label10.Location = New System.Drawing.Point(93, 240)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 13)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Sub Harga"
        '
        'tbSubHarga
        '
        Me.tbSubHarga.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.tbSubHarga.Enabled = False
        Me.tbSubHarga.Location = New System.Drawing.Point(161, 235)
        Me.tbSubHarga.Name = "tbSubHarga"
        Me.tbSubHarga.Size = New System.Drawing.Size(100, 22)
        Me.tbSubHarga.TabIndex = 21
        '
        'rbMakanan
        '
        Me.rbMakanan.AutoSize = True
        Me.rbMakanan.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.rbMakanan.Location = New System.Drawing.Point(96, 108)
        Me.rbMakanan.Name = "rbMakanan"
        Me.rbMakanan.Size = New System.Drawing.Size(70, 17)
        Me.rbMakanan.TabIndex = 3
        Me.rbMakanan.TabStop = True
        Me.rbMakanan.Text = "Makanan"
        Me.rbMakanan.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label5.Location = New System.Drawing.Point(19, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Driver"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label6.Location = New System.Drawing.Point(19, 180)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Jumlah"
        '
        'rbMinuman
        '
        Me.rbMinuman.AutoSize = True
        Me.rbMinuman.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.rbMinuman.Location = New System.Drawing.Point(182, 108)
        Me.rbMinuman.Name = "rbMinuman"
        Me.rbMinuman.Size = New System.Drawing.Size(68, 17)
        Me.rbMinuman.TabIndex = 4
        Me.rbMinuman.TabStop = True
        Me.rbMinuman.Text = "Minuman"
        Me.rbMinuman.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label8.Location = New System.Drawing.Point(19, 110)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 13)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Jenis Menu"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnInsertPlg)
        Me.GroupBox3.Controls.Add(Me.btnResetPesanan)
        Me.GroupBox3.Controls.Add(Me.btnInput)
        Me.GroupBox3.Controls.Add(Me.btnResetPelanggan)
        Me.GroupBox3.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Italic)
        Me.GroupBox3.Location = New System.Drawing.Point(801, 91)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(200, 260)
        Me.GroupBox3.TabIndex = 15
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Pilihan"
        '
        'btnInsertPlg
        '
        Me.btnInsertPlg.BackColor = System.Drawing.Color.Transparent
        Me.btnInsertPlg.BackgroundImage = CType(resources.GetObject("btnInsertPlg.BackgroundImage"), System.Drawing.Image)
        Me.btnInsertPlg.ButtonText = "Input Pelanggan"
        Me.btnInsertPlg.ButtonTextMarginLeft = 0
        Me.btnInsertPlg.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnInsertPlg.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnInsertPlg.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnInsertPlg.DisabledForecolor = System.Drawing.Color.White
        Me.btnInsertPlg.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnInsertPlg.ForeColor = System.Drawing.Color.White
        Me.btnInsertPlg.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnInsertPlg.IconPadding = 10
        Me.btnInsertPlg.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnInsertPlg.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnInsertPlg.IdleBorderRadius = 1
        Me.btnInsertPlg.IdleBorderThickness = 1
        Me.btnInsertPlg.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnInsertPlg.IdleIconLeftImage = Nothing
        Me.btnInsertPlg.IdleIconRightImage = Nothing
        Me.btnInsertPlg.Location = New System.Drawing.Point(37, 38)
        Me.btnInsertPlg.Name = "btnInsertPlg"
        StateProperties1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties1.BorderRadius = 1
        StateProperties1.BorderThickness = 1
        StateProperties1.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties1.ForeColor = System.Drawing.Color.White
        StateProperties1.IconLeftImage = Nothing
        StateProperties1.IconRightImage = Nothing
        Me.btnInsertPlg.onHoverState = StateProperties1
        StateProperties2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties2.BorderRadius = 1
        StateProperties2.BorderThickness = 1
        StateProperties2.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties2.ForeColor = System.Drawing.Color.White
        StateProperties2.IconLeftImage = Nothing
        StateProperties2.IconRightImage = Nothing
        Me.btnInsertPlg.OnPressedState = StateProperties2
        Me.btnInsertPlg.Size = New System.Drawing.Size(129, 43)
        Me.btnInsertPlg.TabIndex = 10
        Me.btnInsertPlg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnResetPesanan
        '
        Me.btnResetPesanan.BackColor = System.Drawing.Color.Transparent
        Me.btnResetPesanan.BackgroundImage = CType(resources.GetObject("btnResetPesanan.BackgroundImage"), System.Drawing.Image)
        Me.btnResetPesanan.ButtonText = "Reset Pesanan"
        Me.btnResetPesanan.ButtonTextMarginLeft = 0
        Me.btnResetPesanan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnResetPesanan.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnResetPesanan.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnResetPesanan.DisabledForecolor = System.Drawing.Color.White
        Me.btnResetPesanan.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnResetPesanan.ForeColor = System.Drawing.Color.White
        Me.btnResetPesanan.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnResetPesanan.IconPadding = 10
        Me.btnResetPesanan.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnResetPesanan.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnResetPesanan.IdleBorderRadius = 1
        Me.btnResetPesanan.IdleBorderThickness = 1
        Me.btnResetPesanan.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnResetPesanan.IdleIconLeftImage = Nothing
        Me.btnResetPesanan.IdleIconRightImage = Nothing
        Me.btnResetPesanan.Location = New System.Drawing.Point(37, 188)
        Me.btnResetPesanan.Name = "btnResetPesanan"
        StateProperties3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties3.BorderRadius = 1
        StateProperties3.BorderThickness = 1
        StateProperties3.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties3.ForeColor = System.Drawing.Color.White
        StateProperties3.IconLeftImage = Nothing
        StateProperties3.IconRightImage = Nothing
        Me.btnResetPesanan.onHoverState = StateProperties3
        StateProperties4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties4.BorderRadius = 1
        StateProperties4.BorderThickness = 1
        StateProperties4.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties4.ForeColor = System.Drawing.Color.White
        StateProperties4.IconLeftImage = Nothing
        StateProperties4.IconRightImage = Nothing
        Me.btnResetPesanan.OnPressedState = StateProperties4
        Me.btnResetPesanan.Size = New System.Drawing.Size(129, 43)
        Me.btnResetPesanan.TabIndex = 13
        Me.btnResetPesanan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnInput
        '
        Me.btnInput.BackColor = System.Drawing.Color.Transparent
        Me.btnInput.BackgroundImage = CType(resources.GetObject("btnInput.BackgroundImage"), System.Drawing.Image)
        Me.btnInput.ButtonText = "Input Pesanan"
        Me.btnInput.ButtonTextMarginLeft = 0
        Me.btnInput.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnInput.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnInput.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnInput.DisabledForecolor = System.Drawing.Color.White
        Me.btnInput.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnInput.ForeColor = System.Drawing.Color.White
        Me.btnInput.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnInput.IconPadding = 10
        Me.btnInput.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnInput.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnInput.IdleBorderRadius = 1
        Me.btnInput.IdleBorderThickness = 1
        Me.btnInput.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnInput.IdleIconLeftImage = Nothing
        Me.btnInput.IdleIconRightImage = Nothing
        Me.btnInput.Location = New System.Drawing.Point(37, 87)
        Me.btnInput.Name = "btnInput"
        StateProperties5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties5.BorderRadius = 1
        StateProperties5.BorderThickness = 1
        StateProperties5.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties5.ForeColor = System.Drawing.Color.White
        StateProperties5.IconLeftImage = Nothing
        StateProperties5.IconRightImage = Nothing
        Me.btnInput.onHoverState = StateProperties5
        StateProperties6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties6.BorderRadius = 1
        StateProperties6.BorderThickness = 1
        StateProperties6.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties6.ForeColor = System.Drawing.Color.White
        StateProperties6.IconLeftImage = Nothing
        StateProperties6.IconRightImage = Nothing
        Me.btnInput.OnPressedState = StateProperties6
        Me.btnInput.Size = New System.Drawing.Size(129, 43)
        Me.btnInput.TabIndex = 11
        Me.btnInput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnResetPelanggan
        '
        Me.btnResetPelanggan.BackColor = System.Drawing.Color.Transparent
        Me.btnResetPelanggan.BackgroundImage = CType(resources.GetObject("btnResetPelanggan.BackgroundImage"), System.Drawing.Image)
        Me.btnResetPelanggan.ButtonText = "Reset Pelanggan"
        Me.btnResetPelanggan.ButtonTextMarginLeft = 0
        Me.btnResetPelanggan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnResetPelanggan.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnResetPelanggan.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnResetPelanggan.DisabledForecolor = System.Drawing.Color.White
        Me.btnResetPelanggan.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnResetPelanggan.ForeColor = System.Drawing.Color.White
        Me.btnResetPelanggan.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnResetPelanggan.IconPadding = 10
        Me.btnResetPelanggan.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnResetPelanggan.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnResetPelanggan.IdleBorderRadius = 1
        Me.btnResetPelanggan.IdleBorderThickness = 1
        Me.btnResetPelanggan.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnResetPelanggan.IdleIconLeftImage = Nothing
        Me.btnResetPelanggan.IdleIconRightImage = Nothing
        Me.btnResetPelanggan.Location = New System.Drawing.Point(37, 139)
        Me.btnResetPelanggan.Name = "btnResetPelanggan"
        StateProperties7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties7.BorderRadius = 1
        StateProperties7.BorderThickness = 1
        StateProperties7.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties7.ForeColor = System.Drawing.Color.White
        StateProperties7.IconLeftImage = Nothing
        StateProperties7.IconRightImage = Nothing
        Me.btnResetPelanggan.onHoverState = StateProperties7
        StateProperties8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties8.BorderRadius = 1
        StateProperties8.BorderThickness = 1
        StateProperties8.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties8.ForeColor = System.Drawing.Color.White
        StateProperties8.IconLeftImage = Nothing
        StateProperties8.IconRightImage = Nothing
        Me.btnResetPelanggan.OnPressedState = StateProperties8
        Me.btnResetPelanggan.Size = New System.Drawing.Size(129, 43)
        Me.btnResetPelanggan.TabIndex = 12
        Me.btnResetPelanggan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvPemesanan)
        Me.GroupBox4.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(194, 357)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(810, 213)
        Me.GroupBox4.TabIndex = 18
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Daftar Pesanan"
        '
        'dgvPemesanan
        '
        Me.dgvPemesanan.AllowCustomTheming = False
        Me.dgvPemesanan.AllowUserToAddRows = False
        Me.dgvPemesanan.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(198, Byte), Integer), CType(CType(227, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        Me.dgvPemesanan.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPemesanan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvPemesanan.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvPemesanan.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvPemesanan.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI Semibold", 11.75!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPemesanan.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvPemesanan.ColumnHeadersHeight = 40
        Me.dgvPemesanan.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(198, Byte), Integer), CType(CType(227, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvPemesanan.CurrentTheme.AlternatingRowsStyle.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.dgvPemesanan.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black
        Me.dgvPemesanan.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvPemesanan.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.White
        Me.dgvPemesanan.CurrentTheme.BackColor = System.Drawing.Color.DodgerBlue
        Me.dgvPemesanan.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(CType(CType(187, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvPemesanan.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue
        Me.dgvPemesanan.CurrentTheme.HeaderStyle.Font = New System.Drawing.Font("Segoe UI Semibold", 11.75!, System.Drawing.FontStyle.Bold)
        Me.dgvPemesanan.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White
        Me.dgvPemesanan.CurrentTheme.Name = Nothing
        Me.dgvPemesanan.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvPemesanan.CurrentTheme.RowsStyle.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.dgvPemesanan.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black
        Me.dgvPemesanan.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvPemesanan.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPemesanan.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvPemesanan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPemesanan.EnableHeadersVisualStyles = False
        Me.dgvPemesanan.GridColor = System.Drawing.Color.FromArgb(CType(CType(187, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvPemesanan.HeaderBackColor = System.Drawing.Color.DodgerBlue
        Me.dgvPemesanan.HeaderBgColor = System.Drawing.Color.Empty
        Me.dgvPemesanan.HeaderForeColor = System.Drawing.Color.White
        Me.dgvPemesanan.Location = New System.Drawing.Point(3, 18)
        Me.dgvPemesanan.Name = "dgvPemesanan"
        Me.dgvPemesanan.ReadOnly = True
        Me.dgvPemesanan.RowHeadersVisible = False
        Me.dgvPemesanan.RowTemplate.Height = 40
        Me.dgvPemesanan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPemesanan.Size = New System.Drawing.Size(804, 192)
        Me.dgvPemesanan.TabIndex = 0
        Me.dgvPemesanan.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.DodgerBlue
        '
        'tbTotalHarga
        '
        Me.tbTotalHarga.AcceptsReturn = False
        Me.tbTotalHarga.AcceptsTab = False
        Me.tbTotalHarga.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.tbTotalHarga.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.tbTotalHarga.BackColor = System.Drawing.Color.White
        Me.tbTotalHarga.BackgroundImage = CType(resources.GetObject("tbTotalHarga.BackgroundImage"), System.Drawing.Image)
        Me.tbTotalHarga.BorderColorActive = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(145, Byte), Integer))
        Me.tbTotalHarga.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.tbTotalHarga.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.tbTotalHarga.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.tbTotalHarga.BorderRadius = 1
        Me.tbTotalHarga.BorderThickness = 2
        Me.tbTotalHarga.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.tbTotalHarga.DefaultFont = New System.Drawing.Font("Segoe UI", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTotalHarga.DefaultText = ""
        Me.tbTotalHarga.Enabled = False
        Me.tbTotalHarga.FillColor = System.Drawing.Color.Gainsboro
        Me.tbTotalHarga.HideSelection = True
        Me.tbTotalHarga.IconLeft = CType(resources.GetObject("tbTotalHarga.IconLeft"), System.Drawing.Image)
        Me.tbTotalHarga.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.tbTotalHarga.IconPadding = 10
        Me.tbTotalHarga.IconRight = Nothing
        Me.tbTotalHarga.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.tbTotalHarga.Location = New System.Drawing.Point(804, 576)
        Me.tbTotalHarga.MaxLength = 32767
        Me.tbTotalHarga.MinimumSize = New System.Drawing.Size(100, 35)
        Me.tbTotalHarga.Modified = False
        Me.tbTotalHarga.Name = "tbTotalHarga"
        Me.tbTotalHarga.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.tbTotalHarga.ReadOnly = False
        Me.tbTotalHarga.SelectedText = ""
        Me.tbTotalHarga.SelectionLength = 0
        Me.tbTotalHarga.SelectionStart = 0
        Me.tbTotalHarga.ShortcutsEnabled = True
        Me.tbTotalHarga.Size = New System.Drawing.Size(200, 42)
        Me.tbTotalHarga.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu
        Me.tbTotalHarga.TabIndex = 20
        Me.tbTotalHarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.tbTotalHarga.TextMarginLeft = 5
        Me.tbTotalHarga.TextPlaceholder = ""
        Me.tbTotalHarga.UseSystemPasswordChar = False
        '
        'DTPWaktu
        '
        Me.DTPWaktu.Location = New System.Drawing.Point(194, 295)
        Me.DTPWaktu.Name = "DTPWaktu"
        Me.DTPWaktu.Size = New System.Drawing.Size(200, 20)
        Me.DTPWaktu.TabIndex = 21
        Me.DTPWaktu.Visible = False
        '
        'FormPesanan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1036, 630)
        Me.ControlBox = False
        Me.Controls.Add(Me.DTPWaktu)
        Me.Controls.Add(Me.tbTotalHarga)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PanelLeftSidebar)
        Me.Controls.Add(Me.PanelNav)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FormPesanan"
        Me.Text = "FormPesanan"
        Me.PanelNav.ResumeLayout(False)
        Me.PanelNav.PerformLayout()
        Me.PanelLeftSidebar.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.jmlPesan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgvPemesanan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PanelNav As Panel
    Friend WithEvents btnMini As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnExit As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents HepiMart As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents PanelLeftSidebar As Panel
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btnHome As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuProgressBar1 As Bunifu.UI.WinForms.BunifuProgressBar
    Friend WithEvents btnDriver As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnMenu As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnPelanggan As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnTransaksi As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnPesanan As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnLogout As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuElipse1 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents tbAlamat As Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents tbTelp As Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents rbMakanan As RadioButton
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents rbMinuman As RadioButton
    Friend WithEvents Label8 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnInsertPlg As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents btnResetPesanan As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents btnInput As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents btnResetPelanggan As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents dgvPemesanan As Bunifu.UI.WinForms.BunifuDataGridView
    Friend WithEvents tbTotalHarga As Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents tbSubHarga As TextBox
    Friend WithEvents BunifuProgressBar2 As Bunifu.UI.WinForms.BunifuProgressBar
    Friend WithEvents Label12 As Label
    Friend WithEvents cbMenu As Bunifu.UI.WinForms.BunifuDropdown
    Friend WithEvents jmlPesan As NumericUpDown
    Friend WithEvents cbDriver As Bunifu.UI.WinForms.BunifuDropdown
    Friend WithEvents Label11 As Label
    Friend WithEvents tbIDPesanan As TextBox
    Friend WithEvents DTPWaktu As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents tbIDpel As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents tbNama As Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox
    Friend WithEvents lblStok As Label
    Friend WithEvents Label7 As Label
End Class
