﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMenu))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim StateProperties1 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties2 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties3 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties4 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties5 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties6 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties7 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties8 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties9 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties10 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties11 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties12 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties13 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties14 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties15 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Dim StateProperties16 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties()
        Me.PanelNav = New System.Windows.Forms.Panel()
        Me.btnMini = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnExit = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.HepiMart = New Bunifu.UI.WinForms.BunifuLabel()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.PanelLeftSidebar = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnHome = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuProgressBar1 = New Bunifu.UI.WinForms.BunifuProgressBar()
        Me.btnDriver = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnMenu = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnPelanggan = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnTransaksi = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.btnPesanan = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnLogout = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.tbNama = New Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbStok = New Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbHarga = New Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox()
        Me.rbMinuman = New System.Windows.Forms.RadioButton()
        Me.rbMakanan = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dataMenu = New Bunifu.UI.WinForms.BunifuDataGridView()
        Me.tbShowID = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnCari = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.tbCari = New Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnSimpan = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.btnTampil = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.btnHapus = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.btnUpdate = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.BunifuElipse1 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnSwitchUpdate = New Bunifu.UI.WinForms.BunifuCheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnStok0 = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.btnLihatStatusMenu = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.btnStok100 = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.PanelNav.SuspendLayout()
        Me.PanelLeftSidebar.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dataMenu, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelNav
        '
        Me.PanelNav.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.PanelNav.Controls.Add(Me.btnMini)
        Me.PanelNav.Controls.Add(Me.btnExit)
        Me.PanelNav.Controls.Add(Me.HepiMart)
        Me.PanelNav.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelNav.Location = New System.Drawing.Point(0, 0)
        Me.PanelNav.Name = "PanelNav"
        Me.PanelNav.Size = New System.Drawing.Size(1117, 45)
        Me.PanelNav.TabIndex = 2
        '
        'btnMini
        '
        Me.btnMini.Activecolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnMini.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnMini.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMini.BorderRadius = 0
        Me.btnMini.ButtonText = ""
        Me.btnMini.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMini.DisabledColor = System.Drawing.Color.Gray
        Me.btnMini.Iconcolor = System.Drawing.Color.Transparent
        Me.btnMini.Iconimage = CType(resources.GetObject("btnMini.Iconimage"), System.Drawing.Image)
        Me.btnMini.Iconimage_right = Nothing
        Me.btnMini.Iconimage_right_Selected = Nothing
        Me.btnMini.Iconimage_Selected = Nothing
        Me.btnMini.IconMarginLeft = 0
        Me.btnMini.IconMarginRight = 0
        Me.btnMini.IconRightVisible = True
        Me.btnMini.IconRightZoom = 0R
        Me.btnMini.IconVisible = True
        Me.btnMini.IconZoom = 90.0R
        Me.btnMini.IsTab = False
        Me.btnMini.Location = New System.Drawing.Point(1017, 3)
        Me.btnMini.Name = "btnMini"
        Me.btnMini.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnMini.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnMini.OnHoverTextColor = System.Drawing.Color.White
        Me.btnMini.selected = False
        Me.btnMini.Size = New System.Drawing.Size(46, 39)
        Me.btnMini.TabIndex = 5
        Me.btnMini.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMini.Textcolor = System.Drawing.Color.White
        Me.btnMini.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'btnExit
        '
        Me.btnExit.Activecolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnExit.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnExit.BorderRadius = 0
        Me.btnExit.ButtonText = ""
        Me.btnExit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnExit.DisabledColor = System.Drawing.Color.Gray
        Me.btnExit.Iconcolor = System.Drawing.Color.Transparent
        Me.btnExit.Iconimage = CType(resources.GetObject("btnExit.Iconimage"), System.Drawing.Image)
        Me.btnExit.Iconimage_right = Nothing
        Me.btnExit.Iconimage_right_Selected = Nothing
        Me.btnExit.Iconimage_Selected = Nothing
        Me.btnExit.IconMarginLeft = 0
        Me.btnExit.IconMarginRight = 0
        Me.btnExit.IconRightVisible = True
        Me.btnExit.IconRightZoom = 0R
        Me.btnExit.IconVisible = True
        Me.btnExit.IconZoom = 90.0R
        Me.btnExit.IsTab = False
        Me.btnExit.Location = New System.Drawing.Point(1059, 3)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnExit.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnExit.OnHoverTextColor = System.Drawing.Color.White
        Me.btnExit.selected = False
        Me.btnExit.Size = New System.Drawing.Size(46, 39)
        Me.btnExit.TabIndex = 4
        Me.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExit.Textcolor = System.Drawing.Color.White
        Me.btnExit.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'HepiMart
        '
        Me.HepiMart.AutoEllipsis = False
        Me.HepiMart.CursorType = Nothing
        Me.HepiMart.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.HepiMart.Location = New System.Drawing.Point(505, 12)
        Me.HepiMart.Name = "HepiMart"
        Me.HepiMart.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.HepiMart.Size = New System.Drawing.Size(80, 23)
        Me.HepiMart.TabIndex = 3
        Me.HepiMart.Text = "Hepi Mart"
        Me.HepiMart.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.HepiMart.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.PanelNav
        Me.BunifuDragControl1.Vertical = True
        '
        'PanelLeftSidebar
        '
        Me.PanelLeftSidebar.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.PanelLeftSidebar.Controls.Add(Me.Panel2)
        Me.PanelLeftSidebar.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelLeftSidebar.Location = New System.Drawing.Point(0, 45)
        Me.PanelLeftSidebar.Name = "PanelLeftSidebar"
        Me.PanelLeftSidebar.Size = New System.Drawing.Size(175, 505)
        Me.PanelLeftSidebar.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Panel2.Controls.Add(Me.btnHome)
        Me.Panel2.Controls.Add(Me.BunifuProgressBar1)
        Me.Panel2.Controls.Add(Me.btnDriver)
        Me.Panel2.Controls.Add(Me.btnMenu)
        Me.Panel2.Controls.Add(Me.btnPelanggan)
        Me.Panel2.Controls.Add(Me.btnTransaksi)
        Me.Panel2.Controls.Add(Me.btnPesanan)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(175, 505)
        Me.Panel2.TabIndex = 1
        '
        'btnHome
        '
        Me.btnHome.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnHome.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHome.BorderRadius = 0
        Me.btnHome.ButtonText = "Dashboard"
        Me.btnHome.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnHome.DisabledColor = System.Drawing.Color.Gray
        Me.btnHome.Iconcolor = System.Drawing.Color.Transparent
        Me.btnHome.Iconimage = CType(resources.GetObject("btnHome.Iconimage"), System.Drawing.Image)
        Me.btnHome.Iconimage_right = Nothing
        Me.btnHome.Iconimage_right_Selected = Nothing
        Me.btnHome.Iconimage_Selected = Nothing
        Me.btnHome.IconMarginLeft = 0
        Me.btnHome.IconMarginRight = 0
        Me.btnHome.IconRightVisible = True
        Me.btnHome.IconRightZoom = 0R
        Me.btnHome.IconVisible = True
        Me.btnHome.IconZoom = 90.0R
        Me.btnHome.IsTab = False
        Me.btnHome.Location = New System.Drawing.Point(-1, 42)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnHome.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnHome.OnHoverTextColor = System.Drawing.Color.White
        Me.btnHome.selected = False
        Me.btnHome.Size = New System.Drawing.Size(176, 48)
        Me.btnHome.TabIndex = 22
        Me.btnHome.Text = "Dashboard"
        Me.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHome.Textcolor = System.Drawing.Color.Black
        Me.btnHome.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'BunifuProgressBar1
        '
        Me.BunifuProgressBar1.Animation = 0
        Me.BunifuProgressBar1.AnimationStep = 10
        Me.BunifuProgressBar1.BackgroundImage = CType(resources.GetObject("BunifuProgressBar1.BackgroundImage"), System.Drawing.Image)
        Me.BunifuProgressBar1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.BunifuProgressBar1.BorderRadius = 5
        Me.BunifuProgressBar1.BorderThickness = 2
        Me.BunifuProgressBar1.Location = New System.Drawing.Point(-7, 451)
        Me.BunifuProgressBar1.MaximumValue = 100
        Me.BunifuProgressBar1.MinimumValue = 0
        Me.BunifuProgressBar1.Name = "BunifuProgressBar1"
        Me.BunifuProgressBar1.ProgressBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.BunifuProgressBar1.ProgressColorLeft = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(181, Byte), Integer))
        Me.BunifuProgressBar1.ProgressColorRight = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(181, Byte), Integer))
        Me.BunifuProgressBar1.Size = New System.Drawing.Size(183, 10)
        Me.BunifuProgressBar1.TabIndex = 3
        Me.BunifuProgressBar1.Value = 0
        '
        'btnDriver
        '
        Me.btnDriver.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnDriver.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnDriver.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDriver.BorderRadius = 0
        Me.btnDriver.ButtonText = "Driver"
        Me.btnDriver.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDriver.DisabledColor = System.Drawing.Color.Gray
        Me.btnDriver.Iconcolor = System.Drawing.Color.Transparent
        Me.btnDriver.Iconimage = CType(resources.GetObject("btnDriver.Iconimage"), System.Drawing.Image)
        Me.btnDriver.Iconimage_right = Nothing
        Me.btnDriver.Iconimage_right_Selected = Nothing
        Me.btnDriver.Iconimage_Selected = Nothing
        Me.btnDriver.IconMarginLeft = 0
        Me.btnDriver.IconMarginRight = 0
        Me.btnDriver.IconRightVisible = True
        Me.btnDriver.IconRightZoom = 0R
        Me.btnDriver.IconVisible = True
        Me.btnDriver.IconZoom = 90.0R
        Me.btnDriver.IsTab = False
        Me.btnDriver.Location = New System.Drawing.Point(0, 312)
        Me.btnDriver.Name = "btnDriver"
        Me.btnDriver.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnDriver.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnDriver.OnHoverTextColor = System.Drawing.Color.White
        Me.btnDriver.selected = False
        Me.btnDriver.Size = New System.Drawing.Size(175, 48)
        Me.btnDriver.TabIndex = 21
        Me.btnDriver.Text = "Driver"
        Me.btnDriver.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDriver.Textcolor = System.Drawing.Color.Black
        Me.btnDriver.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold)
        '
        'btnMenu
        '
        Me.btnMenu.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnMenu.BackColor = System.Drawing.Color.SeaGreen
        Me.btnMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMenu.BorderRadius = 0
        Me.btnMenu.ButtonText = "Menu"
        Me.btnMenu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMenu.DisabledColor = System.Drawing.Color.Gray
        Me.btnMenu.Iconcolor = System.Drawing.Color.Transparent
        Me.btnMenu.Iconimage = CType(resources.GetObject("btnMenu.Iconimage"), System.Drawing.Image)
        Me.btnMenu.Iconimage_right = Nothing
        Me.btnMenu.Iconimage_right_Selected = Nothing
        Me.btnMenu.Iconimage_Selected = Nothing
        Me.btnMenu.IconMarginLeft = 0
        Me.btnMenu.IconMarginRight = 0
        Me.btnMenu.IconRightVisible = True
        Me.btnMenu.IconRightZoom = 0R
        Me.btnMenu.IconVisible = True
        Me.btnMenu.IconZoom = 90.0R
        Me.btnMenu.IsTab = False
        Me.btnMenu.Location = New System.Drawing.Point(0, 258)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Normalcolor = System.Drawing.Color.SeaGreen
        Me.btnMenu.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnMenu.OnHoverTextColor = System.Drawing.Color.White
        Me.btnMenu.selected = False
        Me.btnMenu.Size = New System.Drawing.Size(175, 48)
        Me.btnMenu.TabIndex = 20
        Me.btnMenu.Text = "Menu"
        Me.btnMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu.Textcolor = System.Drawing.Color.Black
        Me.btnMenu.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold)
        '
        'btnPelanggan
        '
        Me.btnPelanggan.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnPelanggan.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnPelanggan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPelanggan.BorderRadius = 0
        Me.btnPelanggan.ButtonText = "Pelanggan"
        Me.btnPelanggan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPelanggan.DisabledColor = System.Drawing.Color.Gray
        Me.btnPelanggan.Iconcolor = System.Drawing.Color.Transparent
        Me.btnPelanggan.Iconimage = CType(resources.GetObject("btnPelanggan.Iconimage"), System.Drawing.Image)
        Me.btnPelanggan.Iconimage_right = Nothing
        Me.btnPelanggan.Iconimage_right_Selected = Nothing
        Me.btnPelanggan.Iconimage_Selected = Nothing
        Me.btnPelanggan.IconMarginLeft = 0
        Me.btnPelanggan.IconMarginRight = 0
        Me.btnPelanggan.IconRightVisible = True
        Me.btnPelanggan.IconRightZoom = 0R
        Me.btnPelanggan.IconVisible = True
        Me.btnPelanggan.IconZoom = 90.0R
        Me.btnPelanggan.IsTab = False
        Me.btnPelanggan.Location = New System.Drawing.Point(0, 204)
        Me.btnPelanggan.Name = "btnPelanggan"
        Me.btnPelanggan.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnPelanggan.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnPelanggan.OnHoverTextColor = System.Drawing.Color.White
        Me.btnPelanggan.selected = False
        Me.btnPelanggan.Size = New System.Drawing.Size(175, 48)
        Me.btnPelanggan.TabIndex = 19
        Me.btnPelanggan.Text = "Pelanggan"
        Me.btnPelanggan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPelanggan.Textcolor = System.Drawing.Color.Black
        Me.btnPelanggan.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold)
        '
        'btnTransaksi
        '
        Me.btnTransaksi.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnTransaksi.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnTransaksi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTransaksi.BorderRadius = 0
        Me.btnTransaksi.ButtonText = "Transaksi"
        Me.btnTransaksi.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnTransaksi.DisabledColor = System.Drawing.Color.Gray
        Me.btnTransaksi.Iconcolor = System.Drawing.Color.Transparent
        Me.btnTransaksi.Iconimage = CType(resources.GetObject("btnTransaksi.Iconimage"), System.Drawing.Image)
        Me.btnTransaksi.Iconimage_right = Nothing
        Me.btnTransaksi.Iconimage_right_Selected = Nothing
        Me.btnTransaksi.Iconimage_Selected = Nothing
        Me.btnTransaksi.IconMarginLeft = 0
        Me.btnTransaksi.IconMarginRight = 0
        Me.btnTransaksi.IconRightVisible = True
        Me.btnTransaksi.IconRightZoom = 0R
        Me.btnTransaksi.IconVisible = True
        Me.btnTransaksi.IconZoom = 90.0R
        Me.btnTransaksi.IsTab = False
        Me.btnTransaksi.Location = New System.Drawing.Point(-1, 150)
        Me.btnTransaksi.Name = "btnTransaksi"
        Me.btnTransaksi.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnTransaksi.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnTransaksi.OnHoverTextColor = System.Drawing.Color.White
        Me.btnTransaksi.selected = False
        Me.btnTransaksi.Size = New System.Drawing.Size(176, 48)
        Me.btnTransaksi.TabIndex = 18
        Me.btnTransaksi.Text = "Transaksi"
        Me.btnTransaksi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTransaksi.Textcolor = System.Drawing.Color.Black
        Me.btnTransaksi.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold)
        '
        'btnPesanan
        '
        Me.btnPesanan.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnPesanan.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnPesanan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPesanan.BorderRadius = 0
        Me.btnPesanan.ButtonText = "Pesanan"
        Me.btnPesanan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPesanan.DisabledColor = System.Drawing.Color.Gray
        Me.btnPesanan.Iconcolor = System.Drawing.Color.Transparent
        Me.btnPesanan.Iconimage = CType(resources.GetObject("btnPesanan.Iconimage"), System.Drawing.Image)
        Me.btnPesanan.Iconimage_right = Nothing
        Me.btnPesanan.Iconimage_right_Selected = Nothing
        Me.btnPesanan.Iconimage_Selected = Nothing
        Me.btnPesanan.IconMarginLeft = 0
        Me.btnPesanan.IconMarginRight = 0
        Me.btnPesanan.IconRightVisible = True
        Me.btnPesanan.IconRightZoom = 0R
        Me.btnPesanan.IconVisible = True
        Me.btnPesanan.IconZoom = 90.0R
        Me.btnPesanan.IsTab = False
        Me.btnPesanan.Location = New System.Drawing.Point(0, 96)
        Me.btnPesanan.Name = "btnPesanan"
        Me.btnPesanan.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnPesanan.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnPesanan.OnHoverTextColor = System.Drawing.Color.White
        Me.btnPesanan.selected = False
        Me.btnPesanan.Size = New System.Drawing.Size(175, 48)
        Me.btnPesanan.TabIndex = 17
        Me.btnPesanan.Text = "Pesanan"
        Me.btnPesanan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPesanan.Textcolor = System.Drawing.Color.Black
        Me.btnPesanan.TextFont = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnLogout)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 451)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(175, 54)
        Me.Panel1.TabIndex = 16
        '
        'btnLogout
        '
        Me.btnLogout.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.btnLogout.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnLogout.BorderRadius = 0
        Me.btnLogout.ButtonText = "Log Out"
        Me.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLogout.DisabledColor = System.Drawing.Color.Gray
        Me.btnLogout.Font = New System.Drawing.Font("Century Gothic", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogout.Iconcolor = System.Drawing.Color.Transparent
        Me.btnLogout.Iconimage = Nothing
        Me.btnLogout.Iconimage_right = Nothing
        Me.btnLogout.Iconimage_right_Selected = Nothing
        Me.btnLogout.Iconimage_Selected = Nothing
        Me.btnLogout.IconMarginLeft = 0
        Me.btnLogout.IconMarginRight = 0
        Me.btnLogout.IconRightVisible = True
        Me.btnLogout.IconRightZoom = 0R
        Me.btnLogout.IconVisible = True
        Me.btnLogout.IconZoom = 90.0R
        Me.btnLogout.IsTab = False
        Me.btnLogout.Location = New System.Drawing.Point(0, 4)
        Me.btnLogout.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.btnLogout.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(123, Byte), Integer))
        Me.btnLogout.OnHoverTextColor = System.Drawing.Color.White
        Me.btnLogout.selected = False
        Me.btnLogout.Size = New System.Drawing.Size(176, 50)
        Me.btnLogout.TabIndex = 0
        Me.btnLogout.Text = "Log Out"
        Me.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnLogout.Textcolor = System.Drawing.Color.Black
        Me.btnLogout.TextFont = New System.Drawing.Font("Century Gothic", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.tbNama)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.tbStok)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.tbHarga)
        Me.Panel3.Controls.Add(Me.rbMinuman)
        Me.Panel3.Controls.Add(Me.rbMakanan)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Location = New System.Drawing.Point(205, 110)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(298, 202)
        Me.Panel3.TabIndex = 4
        '
        'tbNama
        '
        Me.tbNama.AcceptsReturn = False
        Me.tbNama.AcceptsTab = False
        Me.tbNama.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.tbNama.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.tbNama.BackColor = System.Drawing.Color.Transparent
        Me.tbNama.BackgroundImage = CType(resources.GetObject("tbNama.BackgroundImage"), System.Drawing.Image)
        Me.tbNama.BorderColorActive = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(145, Byte), Integer))
        Me.tbNama.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.tbNama.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.tbNama.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.tbNama.BorderRadius = 1
        Me.tbNama.BorderThickness = 2
        Me.tbNama.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.tbNama.DefaultFont = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.tbNama.DefaultText = ""
        Me.tbNama.FillColor = System.Drawing.Color.White
        Me.tbNama.HideSelection = True
        Me.tbNama.IconLeft = Nothing
        Me.tbNama.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.tbNama.IconPadding = 10
        Me.tbNama.IconRight = Nothing
        Me.tbNama.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.tbNama.Location = New System.Drawing.Point(93, 10)
        Me.tbNama.MaxLength = 32767
        Me.tbNama.MinimumSize = New System.Drawing.Size(100, 35)
        Me.tbNama.Modified = False
        Me.tbNama.Name = "tbNama"
        Me.tbNama.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.tbNama.ReadOnly = False
        Me.tbNama.SelectedText = ""
        Me.tbNama.SelectionLength = 0
        Me.tbNama.SelectionStart = 0
        Me.tbNama.ShortcutsEnabled = True
        Me.tbNama.Size = New System.Drawing.Size(176, 35)
        Me.tbNama.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu
        Me.tbNama.TabIndex = 9
        Me.tbNama.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.tbNama.TextMarginLeft = 5
        Me.tbNama.TextPlaceholder = ""
        Me.tbNama.UseSystemPasswordChar = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 147)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Stok"
        '
        'tbStok
        '
        Me.tbStok.AcceptsReturn = False
        Me.tbStok.AcceptsTab = False
        Me.tbStok.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.tbStok.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.tbStok.BackColor = System.Drawing.Color.Transparent
        Me.tbStok.BackgroundImage = CType(resources.GetObject("tbStok.BackgroundImage"), System.Drawing.Image)
        Me.tbStok.BorderColorActive = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(145, Byte), Integer))
        Me.tbStok.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.tbStok.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.tbStok.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.tbStok.BorderRadius = 1
        Me.tbStok.BorderThickness = 2
        Me.tbStok.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.tbStok.DefaultFont = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.tbStok.DefaultText = ""
        Me.tbStok.FillColor = System.Drawing.Color.White
        Me.tbStok.HideSelection = True
        Me.tbStok.IconLeft = Nothing
        Me.tbStok.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.tbStok.IconPadding = 10
        Me.tbStok.IconRight = Nothing
        Me.tbStok.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.tbStok.Location = New System.Drawing.Point(93, 135)
        Me.tbStok.MaxLength = 32767
        Me.tbStok.MinimumSize = New System.Drawing.Size(100, 35)
        Me.tbStok.Modified = False
        Me.tbStok.Name = "tbStok"
        Me.tbStok.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.tbStok.ReadOnly = False
        Me.tbStok.SelectedText = ""
        Me.tbStok.SelectionLength = 0
        Me.tbStok.SelectionStart = 0
        Me.tbStok.ShortcutsEnabled = True
        Me.tbStok.Size = New System.Drawing.Size(176, 35)
        Me.tbStok.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu
        Me.tbStok.TabIndex = 7
        Me.tbStok.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.tbStok.TextMarginLeft = 5
        Me.tbStok.TextPlaceholder = ""
        Me.tbStok.UseSystemPasswordChar = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 106)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Harga"
        '
        'tbHarga
        '
        Me.tbHarga.AcceptsReturn = False
        Me.tbHarga.AcceptsTab = False
        Me.tbHarga.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.tbHarga.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.tbHarga.BackColor = System.Drawing.Color.Transparent
        Me.tbHarga.BackgroundImage = CType(resources.GetObject("tbHarga.BackgroundImage"), System.Drawing.Image)
        Me.tbHarga.BorderColorActive = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(145, Byte), Integer))
        Me.tbHarga.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.tbHarga.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.tbHarga.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.tbHarga.BorderRadius = 1
        Me.tbHarga.BorderThickness = 2
        Me.tbHarga.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.tbHarga.DefaultFont = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.tbHarga.DefaultText = ""
        Me.tbHarga.FillColor = System.Drawing.Color.White
        Me.tbHarga.HideSelection = True
        Me.tbHarga.IconLeft = Nothing
        Me.tbHarga.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.tbHarga.IconPadding = 10
        Me.tbHarga.IconRight = Nothing
        Me.tbHarga.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.tbHarga.Location = New System.Drawing.Point(93, 94)
        Me.tbHarga.MaxLength = 32767
        Me.tbHarga.MinimumSize = New System.Drawing.Size(100, 35)
        Me.tbHarga.Modified = False
        Me.tbHarga.Name = "tbHarga"
        Me.tbHarga.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.tbHarga.ReadOnly = False
        Me.tbHarga.SelectedText = ""
        Me.tbHarga.SelectionLength = 0
        Me.tbHarga.SelectionStart = 0
        Me.tbHarga.ShortcutsEnabled = True
        Me.tbHarga.Size = New System.Drawing.Size(176, 35)
        Me.tbHarga.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu
        Me.tbHarga.TabIndex = 5
        Me.tbHarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.tbHarga.TextMarginLeft = 5
        Me.tbHarga.TextPlaceholder = ""
        Me.tbHarga.UseSystemPasswordChar = False
        '
        'rbMinuman
        '
        Me.rbMinuman.AutoSize = True
        Me.rbMinuman.Location = New System.Drawing.Point(179, 62)
        Me.rbMinuman.Name = "rbMinuman"
        Me.rbMinuman.Size = New System.Drawing.Size(68, 17)
        Me.rbMinuman.TabIndex = 4
        Me.rbMinuman.TabStop = True
        Me.rbMinuman.Text = "Minuman"
        Me.rbMinuman.UseVisualStyleBackColor = True
        '
        'rbMakanan
        '
        Me.rbMakanan.AutoSize = True
        Me.rbMakanan.Location = New System.Drawing.Point(93, 62)
        Me.rbMakanan.Name = "rbMakanan"
        Me.rbMakanan.Size = New System.Drawing.Size(70, 17)
        Me.rbMakanan.TabIndex = 3
        Me.rbMakanan.TabStop = True
        Me.rbMakanan.Text = "Makanan"
        Me.rbMakanan.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Jenis Menu"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Nama Menu"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dataMenu)
        Me.GroupBox1.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(205, 333)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(889, 195)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Daftar Menu"
        '
        'dataMenu
        '
        Me.dataMenu.AllowCustomTheming = False
        Me.dataMenu.AllowUserToAddRows = False
        Me.dataMenu.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(198, Byte), Integer), CType(CType(227, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        Me.dataMenu.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dataMenu.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dataMenu.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dataMenu.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dataMenu.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI Semibold", 11.75!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dataMenu.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dataMenu.ColumnHeadersHeight = 40
        Me.dataMenu.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(198, Byte), Integer), CType(CType(227, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dataMenu.CurrentTheme.AlternatingRowsStyle.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.dataMenu.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black
        Me.dataMenu.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dataMenu.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.White
        Me.dataMenu.CurrentTheme.BackColor = System.Drawing.Color.DodgerBlue
        Me.dataMenu.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(CType(CType(187, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dataMenu.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue
        Me.dataMenu.CurrentTheme.HeaderStyle.Font = New System.Drawing.Font("Segoe UI Semibold", 11.75!, System.Drawing.FontStyle.Bold)
        Me.dataMenu.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White
        Me.dataMenu.CurrentTheme.Name = Nothing
        Me.dataMenu.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dataMenu.CurrentTheme.RowsStyle.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.dataMenu.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black
        Me.dataMenu.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dataMenu.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dataMenu.DefaultCellStyle = DataGridViewCellStyle3
        Me.dataMenu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dataMenu.EnableHeadersVisualStyles = False
        Me.dataMenu.GridColor = System.Drawing.Color.FromArgb(CType(CType(187, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dataMenu.HeaderBackColor = System.Drawing.Color.DodgerBlue
        Me.dataMenu.HeaderBgColor = System.Drawing.Color.Empty
        Me.dataMenu.HeaderForeColor = System.Drawing.Color.White
        Me.dataMenu.Location = New System.Drawing.Point(3, 18)
        Me.dataMenu.Name = "dataMenu"
        Me.dataMenu.ReadOnly = True
        Me.dataMenu.RowHeadersVisible = False
        Me.dataMenu.RowTemplate.Height = 40
        Me.dataMenu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dataMenu.Size = New System.Drawing.Size(883, 174)
        Me.dataMenu.TabIndex = 0
        Me.dataMenu.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.DodgerBlue
        '
        'tbShowID
        '
        Me.tbShowID.Enabled = False
        Me.tbShowID.Location = New System.Drawing.Point(901, 63)
        Me.tbShowID.Name = "tbShowID"
        Me.tbShowID.Size = New System.Drawing.Size(100, 20)
        Me.tbShowID.TabIndex = 6
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnCari)
        Me.GroupBox2.Controls.Add(Me.tbCari)
        Me.GroupBox2.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Italic)
        Me.GroupBox2.Location = New System.Drawing.Point(519, 110)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 119)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cari Menu"
        '
        'btnCari
        '
        Me.btnCari.BackColor = System.Drawing.Color.Transparent
        Me.btnCari.BackgroundImage = CType(resources.GetObject("btnCari.BackgroundImage"), System.Drawing.Image)
        Me.btnCari.ButtonText = "Cari"
        Me.btnCari.ButtonTextMarginLeft = 0
        Me.btnCari.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCari.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnCari.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnCari.DisabledForecolor = System.Drawing.Color.White
        Me.btnCari.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnCari.ForeColor = System.Drawing.Color.White
        Me.btnCari.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnCari.IconPadding = 10
        Me.btnCari.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnCari.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnCari.IdleBorderRadius = 1
        Me.btnCari.IdleBorderThickness = 1
        Me.btnCari.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnCari.IdleIconLeftImage = Nothing
        Me.btnCari.IdleIconRightImage = Nothing
        Me.btnCari.Location = New System.Drawing.Point(52, 74)
        Me.btnCari.Name = "btnCari"
        StateProperties1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties1.BorderRadius = 1
        StateProperties1.BorderThickness = 1
        StateProperties1.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties1.ForeColor = System.Drawing.Color.White
        StateProperties1.IconLeftImage = Nothing
        StateProperties1.IconRightImage = Nothing
        Me.btnCari.onHoverState = StateProperties1
        StateProperties2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties2.BorderRadius = 1
        StateProperties2.BorderThickness = 1
        StateProperties2.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties2.ForeColor = System.Drawing.Color.White
        StateProperties2.IconLeftImage = Nothing
        StateProperties2.IconRightImage = Nothing
        Me.btnCari.OnPressedState = StateProperties2
        Me.btnCari.Size = New System.Drawing.Size(100, 31)
        Me.btnCari.TabIndex = 4
        Me.btnCari.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tbCari
        '
        Me.tbCari.AcceptsReturn = False
        Me.tbCari.AcceptsTab = False
        Me.tbCari.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.tbCari.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.tbCari.BackColor = System.Drawing.Color.Transparent
        Me.tbCari.BackgroundImage = CType(resources.GetObject("tbCari.BackgroundImage"), System.Drawing.Image)
        Me.tbCari.BorderColorActive = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(145, Byte), Integer))
        Me.tbCari.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.tbCari.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.tbCari.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.tbCari.BorderRadius = 1
        Me.tbCari.BorderThickness = 2
        Me.tbCari.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.tbCari.DefaultFont = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.tbCari.DefaultText = ""
        Me.tbCari.FillColor = System.Drawing.Color.White
        Me.tbCari.HideSelection = True
        Me.tbCari.IconLeft = Nothing
        Me.tbCari.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.tbCari.IconPadding = 10
        Me.tbCari.IconRight = Nothing
        Me.tbCari.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.tbCari.Location = New System.Drawing.Point(12, 33)
        Me.tbCari.MaxLength = 32767
        Me.tbCari.MinimumSize = New System.Drawing.Size(100, 35)
        Me.tbCari.Modified = False
        Me.tbCari.Name = "tbCari"
        Me.tbCari.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.tbCari.ReadOnly = False
        Me.tbCari.SelectedText = ""
        Me.tbCari.SelectionLength = 0
        Me.tbCari.SelectionStart = 0
        Me.tbCari.ShortcutsEnabled = True
        Me.tbCari.Size = New System.Drawing.Size(176, 35)
        Me.tbCari.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu
        Me.tbCari.TabIndex = 1
        Me.tbCari.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.tbCari.TextMarginLeft = 5
        Me.tbCari.TextPlaceholder = ""
        Me.tbCari.UseSystemPasswordChar = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(868, 66)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(18, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "ID"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnSimpan)
        Me.GroupBox3.Controls.Add(Me.btnTampil)
        Me.GroupBox3.Controls.Add(Me.btnHapus)
        Me.GroupBox3.Controls.Add(Me.btnUpdate)
        Me.GroupBox3.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Italic)
        Me.GroupBox3.Location = New System.Drawing.Point(736, 110)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(150, 202)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Pilihan"
        '
        'btnSimpan
        '
        Me.btnSimpan.BackColor = System.Drawing.Color.Transparent
        Me.btnSimpan.BackgroundImage = CType(resources.GetObject("btnSimpan.BackgroundImage"), System.Drawing.Image)
        Me.btnSimpan.ButtonText = "Simpan"
        Me.btnSimpan.ButtonTextMarginLeft = 0
        Me.btnSimpan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSimpan.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnSimpan.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnSimpan.DisabledForecolor = System.Drawing.Color.White
        Me.btnSimpan.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnSimpan.ForeColor = System.Drawing.Color.White
        Me.btnSimpan.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnSimpan.IconPadding = 10
        Me.btnSimpan.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnSimpan.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnSimpan.IdleBorderRadius = 1
        Me.btnSimpan.IdleBorderThickness = 1
        Me.btnSimpan.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnSimpan.IdleIconLeftImage = Nothing
        Me.btnSimpan.IdleIconRightImage = Nothing
        Me.btnSimpan.Location = New System.Drawing.Point(19, 31)
        Me.btnSimpan.Name = "btnSimpan"
        StateProperties3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties3.BorderRadius = 1
        StateProperties3.BorderThickness = 1
        StateProperties3.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties3.ForeColor = System.Drawing.Color.White
        StateProperties3.IconLeftImage = Nothing
        StateProperties3.IconRightImage = Nothing
        Me.btnSimpan.onHoverState = StateProperties3
        StateProperties4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties4.BorderRadius = 1
        StateProperties4.BorderThickness = 1
        StateProperties4.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties4.ForeColor = System.Drawing.Color.White
        StateProperties4.IconLeftImage = Nothing
        StateProperties4.IconRightImage = Nothing
        Me.btnSimpan.OnPressedState = StateProperties4
        Me.btnSimpan.Size = New System.Drawing.Size(107, 31)
        Me.btnSimpan.TabIndex = 10
        Me.btnSimpan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnTampil
        '
        Me.btnTampil.BackColor = System.Drawing.Color.Transparent
        Me.btnTampil.BackgroundImage = CType(resources.GetObject("btnTampil.BackgroundImage"), System.Drawing.Image)
        Me.btnTampil.ButtonText = "Tampilkan Data"
        Me.btnTampil.ButtonTextMarginLeft = 0
        Me.btnTampil.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnTampil.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnTampil.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnTampil.DisabledForecolor = System.Drawing.Color.White
        Me.btnTampil.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnTampil.ForeColor = System.Drawing.Color.White
        Me.btnTampil.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnTampil.IconPadding = 10
        Me.btnTampil.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnTampil.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnTampil.IdleBorderRadius = 1
        Me.btnTampil.IdleBorderThickness = 1
        Me.btnTampil.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnTampil.IdleIconLeftImage = Nothing
        Me.btnTampil.IdleIconRightImage = Nothing
        Me.btnTampil.Location = New System.Drawing.Point(19, 150)
        Me.btnTampil.Name = "btnTampil"
        StateProperties5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties5.BorderRadius = 1
        StateProperties5.BorderThickness = 1
        StateProperties5.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties5.ForeColor = System.Drawing.Color.White
        StateProperties5.IconLeftImage = Nothing
        StateProperties5.IconRightImage = Nothing
        Me.btnTampil.onHoverState = StateProperties5
        StateProperties6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties6.BorderRadius = 1
        StateProperties6.BorderThickness = 1
        StateProperties6.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties6.ForeColor = System.Drawing.Color.White
        StateProperties6.IconLeftImage = Nothing
        StateProperties6.IconRightImage = Nothing
        Me.btnTampil.OnPressedState = StateProperties6
        Me.btnTampil.Size = New System.Drawing.Size(107, 31)
        Me.btnTampil.TabIndex = 13
        Me.btnTampil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnHapus
        '
        Me.btnHapus.BackColor = System.Drawing.Color.Transparent
        Me.btnHapus.BackgroundImage = CType(resources.GetObject("btnHapus.BackgroundImage"), System.Drawing.Image)
        Me.btnHapus.ButtonText = "Hapus"
        Me.btnHapus.ButtonTextMarginLeft = 0
        Me.btnHapus.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnHapus.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnHapus.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnHapus.DisabledForecolor = System.Drawing.Color.White
        Me.btnHapus.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnHapus.ForeColor = System.Drawing.Color.White
        Me.btnHapus.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnHapus.IconPadding = 10
        Me.btnHapus.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnHapus.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnHapus.IdleBorderRadius = 1
        Me.btnHapus.IdleBorderThickness = 1
        Me.btnHapus.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnHapus.IdleIconLeftImage = Nothing
        Me.btnHapus.IdleIconRightImage = Nothing
        Me.btnHapus.Location = New System.Drawing.Point(19, 69)
        Me.btnHapus.Name = "btnHapus"
        StateProperties7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties7.BorderRadius = 1
        StateProperties7.BorderThickness = 1
        StateProperties7.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties7.ForeColor = System.Drawing.Color.White
        StateProperties7.IconLeftImage = Nothing
        StateProperties7.IconRightImage = Nothing
        Me.btnHapus.onHoverState = StateProperties7
        StateProperties8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties8.BorderRadius = 1
        StateProperties8.BorderThickness = 1
        StateProperties8.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties8.ForeColor = System.Drawing.Color.White
        StateProperties8.IconLeftImage = Nothing
        StateProperties8.IconRightImage = Nothing
        Me.btnHapus.OnPressedState = StateProperties8
        Me.btnHapus.Size = New System.Drawing.Size(107, 31)
        Me.btnHapus.TabIndex = 11
        Me.btnHapus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnUpdate
        '
        Me.btnUpdate.BackColor = System.Drawing.Color.Transparent
        Me.btnUpdate.BackgroundImage = CType(resources.GetObject("btnUpdate.BackgroundImage"), System.Drawing.Image)
        Me.btnUpdate.ButtonText = "Update"
        Me.btnUpdate.ButtonTextMarginLeft = 0
        Me.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdate.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnUpdate.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnUpdate.DisabledForecolor = System.Drawing.Color.White
        Me.btnUpdate.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnUpdate.ForeColor = System.Drawing.Color.White
        Me.btnUpdate.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdate.IconPadding = 10
        Me.btnUpdate.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdate.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnUpdate.IdleBorderRadius = 1
        Me.btnUpdate.IdleBorderThickness = 1
        Me.btnUpdate.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnUpdate.IdleIconLeftImage = Nothing
        Me.btnUpdate.IdleIconRightImage = Nothing
        Me.btnUpdate.Location = New System.Drawing.Point(19, 110)
        Me.btnUpdate.Name = "btnUpdate"
        StateProperties9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties9.BorderRadius = 1
        StateProperties9.BorderThickness = 1
        StateProperties9.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties9.ForeColor = System.Drawing.Color.White
        StateProperties9.IconLeftImage = Nothing
        StateProperties9.IconRightImage = Nothing
        Me.btnUpdate.onHoverState = StateProperties9
        StateProperties10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties10.BorderRadius = 1
        StateProperties10.BorderThickness = 1
        StateProperties10.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties10.ForeColor = System.Drawing.Color.White
        StateProperties10.IconLeftImage = Nothing
        StateProperties10.IconRightImage = Nothing
        Me.btnUpdate.OnPressedState = StateProperties10
        Me.btnUpdate.Size = New System.Drawing.Size(107, 31)
        Me.btnUpdate.TabIndex = 12
        Me.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BunifuElipse1
        '
        Me.BunifuElipse1.ElipseRadius = 5
        Me.BunifuElipse1.TargetControl = Me
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.btnSwitchUpdate)
        Me.GroupBox4.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Italic)
        Me.GroupBox4.Location = New System.Drawing.Point(519, 232)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(200, 80)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Update"
        '
        'btnSwitchUpdate
        '
        Me.btnSwitchUpdate.AllowBindingControlAnimation = True
        Me.btnSwitchUpdate.AllowBindingControlColorChanges = False
        Me.btnSwitchUpdate.AllowBindingControlLocation = True
        Me.btnSwitchUpdate.AllowCheckBoxAnimation = False
        Me.btnSwitchUpdate.AllowCheckmarkAnimation = True
        Me.btnSwitchUpdate.AllowOnHoverStates = True
        Me.btnSwitchUpdate.AutoCheck = True
        Me.btnSwitchUpdate.BackColor = System.Drawing.Color.Transparent
        Me.btnSwitchUpdate.BackgroundImage = CType(resources.GetObject("btnSwitchUpdate.BackgroundImage"), System.Drawing.Image)
        Me.btnSwitchUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnSwitchUpdate.BindingControl = Nothing
        Me.btnSwitchUpdate.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right
        Me.btnSwitchUpdate.Checked = True
        Me.btnSwitchUpdate.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Checked
        Me.btnSwitchUpdate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSwitchUpdate.CustomCheckmarkImage = Nothing
        Me.btnSwitchUpdate.Location = New System.Drawing.Point(85, 25)
        Me.btnSwitchUpdate.MinimumSize = New System.Drawing.Size(17, 17)
        Me.btnSwitchUpdate.Name = "btnSwitchUpdate"
        Me.btnSwitchUpdate.OnCheck.BorderColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(155, Byte), Integer))
        Me.btnSwitchUpdate.OnCheck.BorderRadius = 2
        Me.btnSwitchUpdate.OnCheck.BorderThickness = 2
        Me.btnSwitchUpdate.OnCheck.CheckBoxColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(155, Byte), Integer))
        Me.btnSwitchUpdate.OnCheck.CheckmarkColor = System.Drawing.Color.White
        Me.btnSwitchUpdate.OnCheck.CheckmarkThickness = 2
        Me.btnSwitchUpdate.OnDisable.BorderColor = System.Drawing.Color.LightGray
        Me.btnSwitchUpdate.OnDisable.BorderRadius = 2
        Me.btnSwitchUpdate.OnDisable.BorderThickness = 2
        Me.btnSwitchUpdate.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent
        Me.btnSwitchUpdate.OnDisable.CheckmarkColor = System.Drawing.Color.LightGray
        Me.btnSwitchUpdate.OnDisable.CheckmarkThickness = 2
        Me.btnSwitchUpdate.OnHoverChecked.BorderColor = System.Drawing.Color.FromArgb(CType(CType(151, Byte), Integer), CType(CType(131, Byte), Integer), CType(CType(188, Byte), Integer))
        Me.btnSwitchUpdate.OnHoverChecked.BorderRadius = 2
        Me.btnSwitchUpdate.OnHoverChecked.BorderThickness = 2
        Me.btnSwitchUpdate.OnHoverChecked.CheckBoxColor = System.Drawing.Color.FromArgb(CType(CType(151, Byte), Integer), CType(CType(131, Byte), Integer), CType(CType(188, Byte), Integer))
        Me.btnSwitchUpdate.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White
        Me.btnSwitchUpdate.OnHoverChecked.CheckmarkThickness = 2
        Me.btnSwitchUpdate.OnHoverUnchecked.BorderColor = System.Drawing.Color.FromArgb(CType(CType(151, Byte), Integer), CType(CType(131, Byte), Integer), CType(CType(188, Byte), Integer))
        Me.btnSwitchUpdate.OnHoverUnchecked.BorderRadius = 2
        Me.btnSwitchUpdate.OnHoverUnchecked.BorderThickness = 2
        Me.btnSwitchUpdate.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent
        Me.btnSwitchUpdate.OnUncheck.BorderColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(155, Byte), Integer))
        Me.btnSwitchUpdate.OnUncheck.BorderRadius = 2
        Me.btnSwitchUpdate.OnUncheck.BorderThickness = 2
        Me.btnSwitchUpdate.OnUncheck.CheckBoxColor = System.Drawing.Color.Transparent
        Me.btnSwitchUpdate.Size = New System.Drawing.Size(33, 33)
        Me.btnSwitchUpdate.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Bunifu
        Me.btnSwitchUpdate.TabIndex = 15
        Me.btnSwitchUpdate.ThreeState = False
        Me.btnSwitchUpdate.ToolTipText = Nothing
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.btnStok0)
        Me.GroupBox5.Controls.Add(Me.btnLihatStatusMenu)
        Me.GroupBox5.Controls.Add(Me.btnStok100)
        Me.GroupBox5.Location = New System.Drawing.Point(901, 110)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(193, 202)
        Me.GroupBox5.TabIndex = 16
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Tools Menu"
        '
        'btnStok0
        '
        Me.btnStok0.BackColor = System.Drawing.Color.Transparent
        Me.btnStok0.BackgroundImage = CType(resources.GetObject("btnStok0.BackgroundImage"), System.Drawing.Image)
        Me.btnStok0.ButtonText = "Kembalikan Stok (0)"
        Me.btnStok0.ButtonTextMarginLeft = 0
        Me.btnStok0.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnStok0.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnStok0.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnStok0.DisabledForecolor = System.Drawing.Color.White
        Me.btnStok0.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnStok0.ForeColor = System.Drawing.Color.White
        Me.btnStok0.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnStok0.IconPadding = 10
        Me.btnStok0.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnStok0.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnStok0.IdleBorderRadius = 1
        Me.btnStok0.IdleBorderThickness = 1
        Me.btnStok0.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnStok0.IdleIconLeftImage = Nothing
        Me.btnStok0.IdleIconRightImage = Nothing
        Me.btnStok0.Location = New System.Drawing.Point(24, 129)
        Me.btnStok0.Name = "btnStok0"
        StateProperties11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties11.BorderRadius = 1
        StateProperties11.BorderThickness = 1
        StateProperties11.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties11.ForeColor = System.Drawing.Color.White
        StateProperties11.IconLeftImage = Nothing
        StateProperties11.IconRightImage = Nothing
        Me.btnStok0.onHoverState = StateProperties11
        StateProperties12.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties12.BorderRadius = 1
        StateProperties12.BorderThickness = 1
        StateProperties12.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties12.ForeColor = System.Drawing.Color.White
        StateProperties12.IconLeftImage = Nothing
        StateProperties12.IconRightImage = Nothing
        Me.btnStok0.OnPressedState = StateProperties12
        Me.btnStok0.Size = New System.Drawing.Size(147, 31)
        Me.btnStok0.TabIndex = 14
        Me.btnStok0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnLihatStatusMenu
        '
        Me.btnLihatStatusMenu.BackColor = System.Drawing.Color.Transparent
        Me.btnLihatStatusMenu.BackgroundImage = CType(resources.GetObject("btnLihatStatusMenu.BackgroundImage"), System.Drawing.Image)
        Me.btnLihatStatusMenu.ButtonText = "Menu Tidak Aktif"
        Me.btnLihatStatusMenu.ButtonTextMarginLeft = 0
        Me.btnLihatStatusMenu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLihatStatusMenu.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnLihatStatusMenu.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnLihatStatusMenu.DisabledForecolor = System.Drawing.Color.White
        Me.btnLihatStatusMenu.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnLihatStatusMenu.ForeColor = System.Drawing.Color.White
        Me.btnLihatStatusMenu.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnLihatStatusMenu.IconPadding = 10
        Me.btnLihatStatusMenu.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnLihatStatusMenu.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnLihatStatusMenu.IdleBorderRadius = 1
        Me.btnLihatStatusMenu.IdleBorderThickness = 1
        Me.btnLihatStatusMenu.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnLihatStatusMenu.IdleIconLeftImage = Nothing
        Me.btnLihatStatusMenu.IdleIconRightImage = Nothing
        Me.btnLihatStatusMenu.Location = New System.Drawing.Point(24, 37)
        Me.btnLihatStatusMenu.Name = "btnLihatStatusMenu"
        StateProperties13.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties13.BorderRadius = 1
        StateProperties13.BorderThickness = 1
        StateProperties13.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties13.ForeColor = System.Drawing.Color.White
        StateProperties13.IconLeftImage = Nothing
        StateProperties13.IconRightImage = Nothing
        Me.btnLihatStatusMenu.onHoverState = StateProperties13
        StateProperties14.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties14.BorderRadius = 1
        StateProperties14.BorderThickness = 1
        StateProperties14.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties14.ForeColor = System.Drawing.Color.White
        StateProperties14.IconLeftImage = Nothing
        StateProperties14.IconRightImage = Nothing
        Me.btnLihatStatusMenu.OnPressedState = StateProperties14
        Me.btnLihatStatusMenu.Size = New System.Drawing.Size(147, 31)
        Me.btnLihatStatusMenu.TabIndex = 12
        Me.btnLihatStatusMenu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnStok100
        '
        Me.btnStok100.BackColor = System.Drawing.Color.Transparent
        Me.btnStok100.BackgroundImage = CType(resources.GetObject("btnStok100.BackgroundImage"), System.Drawing.Image)
        Me.btnStok100.ButtonText = "Kembalikan Stok (100)"
        Me.btnStok100.ButtonTextMarginLeft = 0
        Me.btnStok100.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnStok100.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnStok100.DisabledFillColor = System.Drawing.Color.Gray
        Me.btnStok100.DisabledForecolor = System.Drawing.Color.White
        Me.btnStok100.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!)
        Me.btnStok100.ForeColor = System.Drawing.Color.White
        Me.btnStok100.IconLeftCursor = System.Windows.Forms.Cursors.Hand
        Me.btnStok100.IconPadding = 10
        Me.btnStok100.IconRightCursor = System.Windows.Forms.Cursors.Hand
        Me.btnStok100.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnStok100.IdleBorderRadius = 1
        Me.btnStok100.IdleBorderThickness = 1
        Me.btnStok100.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(183, Byte), Integer))
        Me.btnStok100.IdleIconLeftImage = Nothing
        Me.btnStok100.IdleIconRightImage = Nothing
        Me.btnStok100.Location = New System.Drawing.Point(24, 85)
        Me.btnStok100.Name = "btnStok100"
        StateProperties15.BorderColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties15.BorderRadius = 1
        StateProperties15.BorderThickness = 1
        StateProperties15.FillColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(221, Byte), Integer))
        StateProperties15.ForeColor = System.Drawing.Color.White
        StateProperties15.IconLeftImage = Nothing
        StateProperties15.IconRightImage = Nothing
        Me.btnStok100.onHoverState = StateProperties15
        StateProperties16.BorderColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties16.BorderRadius = 1
        StateProperties16.BorderThickness = 1
        StateProperties16.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(144, Byte), Integer))
        StateProperties16.ForeColor = System.Drawing.Color.White
        StateProperties16.IconLeftImage = Nothing
        StateProperties16.IconRightImage = Nothing
        Me.btnStok100.OnPressedState = StateProperties16
        Me.btnStok100.Size = New System.Drawing.Size(147, 31)
        Me.btnStok100.TabIndex = 13
        Me.btnStok100.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FormMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1117, 550)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.tbShowID)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.PanelLeftSidebar)
        Me.Controls.Add(Me.PanelNav)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FormMenu"
        Me.Text = "FormMenu"
        Me.PanelNav.ResumeLayout(False)
        Me.PanelNav.PerformLayout()
        Me.PanelLeftSidebar.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dataMenu, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PanelNav As Panel
    Friend WithEvents btnMini As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnExit As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents HepiMart As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
    Friend WithEvents PanelLeftSidebar As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btnHome As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuProgressBar1 As Bunifu.UI.WinForms.BunifuProgressBar
    Friend WithEvents btnDriver As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnMenu As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnPelanggan As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnTransaksi As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents btnPesanan As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnLogout As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel3 As Panel
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents tbStok As Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents tbHarga As Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox
    Friend WithEvents rbMinuman As RadioButton
    Friend WithEvents rbMakanan As RadioButton
    Friend WithEvents Label2 As Label
    Friend WithEvents tbShowID As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents tbCari As Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox
    Friend WithEvents tbNama As Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox
    Friend WithEvents btnCari As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents Label5 As Label
    Friend WithEvents dataMenu As Bunifu.UI.WinForms.BunifuDataGridView
    Friend WithEvents btnTampil As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents btnUpdate As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents btnHapus As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents btnSimpan As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents BunifuElipse1 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents btnSwitchUpdate As Bunifu.UI.WinForms.BunifuCheckBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents btnStok0 As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents btnLihatStatusMenu As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents btnStok100 As Bunifu.UI.WinForms.BunifuButton.BunifuButton
End Class
