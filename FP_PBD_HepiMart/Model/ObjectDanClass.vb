﻿Module ObjectDanClass
    Public FMenu As New FormMenu
    Public FPesanan As New FormPesanan
    Public FDriver As New FormDriver
    Public FTransaksi As New FormTransaksi
    Public FHome As New Home
    Public FPelanggan As New FormPelanggan
    Public FLogin As New Login
    Public FReport As New FormReport
    'FORM
    Public EntitasDriver As New EntDriver
    Public ControlDriver As New ContDriver
    Public EntitasMenu As New EntMenu
    Public ControlMenu As New ContMenu
    Public rand As New Random()
    Public EntitasPel As New EntPelanggan
    Public ControlPel As New ContPelanggan
    Public opf As New OpenFileDialog
    Public EntitasAdmin As New EntAdmin
    Public ControlAdmin As New ContAdmin
    Public EntitasPesanan As New EntPesanan
    Public ControlPesanan As New ContPesanan
    Public EntitasTransaksi As New EntTransaksi
    Public ControlTransaksi As New ContTransaksi
    Public EntitasHome As New EntHome
    Public ControlHome As New ContHome
    Public FTM As New FormToolsMenu
    Public FTD As New FormToolsDriver
    Public ControlFTD As New ContFTD
    Public ControlFTM As New ContFTM
End Module
